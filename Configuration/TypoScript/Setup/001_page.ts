page {
    ## Modify <body>
    bodyTag >
    bodyTagCObject = CASE
    bodyTagCObject {
        key.data = pagelayout

        basic = COA
        basic {
            10 = TEXT
            10.data = levelfield:0, uid
            10.dataWrap =  id="page-{TSFE:id}" class="be-user-{TSFE:beUserLogin} lang-{TSFE:sys_language_uid} root-|

            20 = TEXT
            20.data = levelfield:1, uid
            20.noTrimWrap = | parent-||

            30 = TEXT
            30.data = level:1
            30.noTrimWrap = | level-||

            40 = TEXT
            40 {
                data = page:backend_layout
                ifEmpty.data = levelfield :-1, backend_layout_next_level, slide
                noTrimWrap = | be-layout-||

                stdWrap.replacement {
                    10 {
                        search = pagets__
                        replace =
                    }
                    20 {
                        search = #_[^_]+#
                        replace =
                        useRegExp = 1
                    }
                }
            }

            50 = TEXT
            50 {
                intval = 1
                data = page:layout
                noTrimWrap = | fe-layout-| |
            }

            60 = TEXT
            60.value = has-sub
            60.if.isTrue.numRows {
                table = pages
                where = pid=this
            }

            70 = TEXT
            70.value = no-sub
            70.if.negate = 1
            70.if.isTrue.numRows {
                table = pages
                where = pid=this
            }

            80 = TEXT
            80.value = "

            stdWrap.noTrimWrap = |<body |>|
        }

        default < .basic

    }
}