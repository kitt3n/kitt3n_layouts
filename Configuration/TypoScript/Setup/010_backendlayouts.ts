## PAGE FLUIDTEMPLATE
[userFunc = TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('kitt3n_custom')]
    page = PAGE
    page {
        10 = FLUIDTEMPLATE
        10 {
            templateName = TEXT
            templateName.stdWrap {
                cObject = TEXT
                cObject {
                    data = levelfield:-2,backend_layout_next_level,slide
                    override.field = backend_layout
                    split {
                        token = pagets__
                        1.current = 1
                        1.wrap = |
                    }
                }
                ifEmpty = 0
            }
            layoutRootPaths {
                20 = EXT:kitt3n_layouts/Resources/Private/Layouts/
                30 = EXT:kitt3n_custom/Resources/Private/Layouts/
            }
            partialRootPaths {
                20 = EXT:kitt3n_layouts/Resources/Private/Partials/
                30 = EXT:kitt3n_custom/Resources/Private/Partials/
            }
            templateRootPaths {
                20 = EXT:kitt3n_layouts/Resources/Private/Templates/BackendLayouts/
                30 = EXT:kitt3n_custom/Resources/Private/Templates/BackendLayouts/
            }
        }
    }
[global]