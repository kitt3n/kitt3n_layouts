## Basic backend layouts -> EXT:kitt3n_layouts
<INCLUDE_TYPOSCRIPT: source="DIR:EXT:kitt3n_layouts/Configuration/TSconfig/BackendLayouts" extensions="ts,txt">

## Custom backend layouts -> EXT:kitt3n_custom
[userFunc = TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('kitt3n_custom')]
    <INCLUDE_TYPOSCRIPT: source="DIR:EXT:kitt3n_custom/Configuration/TSconfig/BackendLayouts" extensions="ts,txt">
[global]
