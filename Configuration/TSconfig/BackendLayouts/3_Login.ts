## Login backend layout (3)
mod {
    web_layout {
        BackendLayouts {
            ## Login 1 row
            3_Login {
                title = Login
                icon = EXT:kitt3n_layouts/Resources/Public/Icons/BackendLayouts/be_login.png
                config {
                    backend_layout {
                        colCount = 1
                        rowCount = 1
                        rows {
                            1 {
                                columns {
                                    1 {
                                        name = Inhalt
                                        colPos = 0
                                        colspan = 1
                                    }
                                }
                            }
                        }
                    }
                }
            }

        }
    }
}