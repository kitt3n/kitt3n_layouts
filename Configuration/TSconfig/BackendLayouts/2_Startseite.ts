## Home backend layout (2)
mod {
    web_layout {
        BackendLayouts {
            ## Homepage 2 rows
            2_Startseite {
                title = Startseite
                icon = EXT:kitt3n_layouts/Resources/Public/Icons/BackendLayouts/be_home.png
                config {
                    backend_layout {
                        colCount = 1
                        rowCount = 2
                        rows {
                            1 {
                                columns {
                                    1 {
                                        name = Header
                                        colPos = 1
                                        colspan = 1
                                        # allowed = mask_header,mask_headerlogin,mask_keyvisual
                                    }
                                }
                            }
                            2 {
                                columns {
                                    1 {
                                        name = Inhalt
                                        colPos = 0
                                        colspan = 1
                                    }
                                }
                            }
                        }
                    }
                }
            }

        }
    }
}