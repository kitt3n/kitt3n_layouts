<?php
defined('TYPO3_MODE') || die('Access denied.');

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder
$boot = function () {

    if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('kitt3n_layouts')) {

        ## Add PAGE TSConfig
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="DIR:EXT:kitt3n_layouts/Configuration/TSconfig/Page" extensions="ts,txt">');

        $rootLineFields = &$GLOBALS["TYPO3_CONF_VARS"]["FE"]["addRootLineFields"];
        if($rootLineFields != '') {
            $rootLineFields .= ',';
        }
        $rootLineFields .= 'backend_layout,backend_layout_next_level';


    }

};

$boot();
unset($boot);